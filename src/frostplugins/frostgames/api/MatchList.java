package frostplugins.frostgames.api;

import java.util.ArrayList;
import java.util.List;

/**
 * A class to store all matches
 * 
 * @author F3R05
 */
public class MatchList {
	
	/**
	 * Stores all the matches
	 */
	private List<Match> matches;
	
	/**
	 * Constructor
	 */
	public MatchList() {
		matches = new ArrayList<Match>();
	}
	
	/**
	 * Adds a match to the match list
	 * @param match The match to be added
	 */
	public void addMatch(Match match) {
		matches.add(match);
	}
	
	/**
	 * Removes a match from the match list
	 * @param match The match to be removed
	 */
	public void removeMatch(Match match) {
		matches.remove(match);
	}
	
	/**
	 * Gets the stored matches
	 * @return The matches
	 */
	public List<Match> getMatches() {
		return matches;
	}
	
}
