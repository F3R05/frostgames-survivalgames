package frostplugins.frostgames.api;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

/**
 * A class to store players (e.g. players in a lobby, etc)
 * 
 * @author F3R05
 */
public class PlayerList {
	
	/**
	 * Stores the players
	 */
	private List<Player> playerlist;
	
	/**
	 * Constructor
	 */
	public PlayerList() {
		playerlist = new ArrayList<Player>();
	}
	
	/**
	 * Adds a player to the player list
	 * @param player The player to be added
	 */
	public void addPlayer(Player player) {
		if(!playerlist.contains(player)) {
			playerlist.add(player);
		}
	}
	
	/**
	 * Removes a player from the player list
	 * @param player The player to be removed
	 */
	public void removePlayer(Player player) {
		playerlist.remove(player);
	}
	
	/**
	 * Removes a player from the player list
	 * @param name The name of the player to be removed
	 */
	public void removePlayer(String name) {
		for(Player player : playerlist) {
			if(player.getName().equals(name)) {
				playerlist.remove(player);
			}
		}
	}
	
	/**
	 * Gets the player list
	 * @return The player list
	 */
	public List<Player> getPlayers() {
		return playerlist;
	}

}
