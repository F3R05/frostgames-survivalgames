package frostplugins.frostgames.core;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;

import frostplugins.frostgames.api.IngamePlayerDeathEvent;
import frostplugins.frostgames.api.Match;
import frostplugins.frostgames.api.MatchList;

public class CorePlugin extends JavaPlugin implements Listener {
	
	private MatchList matchlist;
	
	/**
	 * When the plugin gets enabled
	 */
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
		matchlist = new MatchList();
	}
	
	/**
	 * When the plugin gets disabled
	 */
	@Override
	public void onDisable() {
		
	}
	
	/**
	 * Called when a player dies
	 * @param event The event
	 */
	@EventHandler
	public void onPlayerDied(PlayerDeathEvent event) {
		for(Match m : matchlist.getMatches()) {
			if(m.getParticipants().getPlayers().contains(event.getEntity())) {
				getServer().getPluginManager().callEvent(new IngamePlayerDeathEvent(event, m));
			}
		}
	}
	
}
