package frostplugins.frostgames.api;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 * Event which is called when an ingame player dies
 * @author F3R05
 */
public class IngamePlayerDeathEvent extends Event {
	
	// Required for bukkit events
	/**
	 * The handler list
	 */
	private static final HandlerList handlers = new HandlerList();
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	/**
	 * Gets the handler list
	 * @return the handler list
	 */
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	/**
	 * The death event called by bukkit when a player dies
	 */
	private PlayerDeathEvent deathevent;
	
	/**
	 * The match in which the player died
	 */
	private Match match;
	
	/**
	 * Constructor
	 * @param deathevent The death event called by bukkit when a player dies
	 * @param match The match in which the player died
	 */
	public IngamePlayerDeathEvent(PlayerDeathEvent deathevent, Match match) {
		this.deathevent = deathevent;
		this.match = match;
	}
	
	/**
	 * Gets the player death event called by bukkit
	 * @return The PlayerDeathEvent called by bukkit
	 */
	public PlayerDeathEvent getPlayerDeathEvent() {
		return deathevent;
	}
	
	/**
	 * Gets the match in which the player died
	 * @return The match in which the player died
	 */
	public Match getMatch() {
		return match;
	}	
}
