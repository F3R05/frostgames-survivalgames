package frostplugins.frostgames.api;


import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * A match of the mini game
 * 
 * @author F3R05
 */
public class Match {
	
	/**
	 * The name of the match
	 */
	private String name;
	
	/**
	 * The maximum number of players in the match
	 */
	private int maxplayers;
	
	/**
	 * The spawn for players who joined the match
	 */
	private Location lobbyspawn;
	
	/**
	 * If the match is running or if it is waiting for players
	 */
	private boolean ingame = false;
	
	/**
	 * The participants of the match
	 */
	private PlayerList participants;
	
	/**
	 * The time of the countdown
	 */
	private double countdowntime;
	
	/**
	 * The plugin to which the match belongs
	 */
	private MiniGamesPlugin plugin;
	
	
	/**
	 * Constructor
	 * @param name The (unique) name of the match
	 * @param maxplayers The maximum number of players in the match
	 * @param lobbyspawn The spawn for players who joined the match
	 */
	public Match(String name, int maxplayers, Location lobbyspawn, MiniGamesPlugin plugin) {
		participants = new PlayerList();
		this.name = name;
		this.maxplayers = maxplayers;
		this.lobbyspawn = lobbyspawn;
		this.plugin = plugin;
	}
	
	/**
	 * Sets the name of the match
	 * @param name The name to be set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the name of the match
	 * @return The name of the match
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the lobby spawn
	 * @param location The lobby spawn to be set
	 */
	public void setLobbySpawn(Location location) {
		lobbyspawn = location;
	}
	
	/**
	 * Gets the lobby spawn
	 * @return The lobby spawn
	 */
	public Location getLobbySpawn() {
		return lobbyspawn;
	}
	
	/**
	 * Sets the countdown time
	 * @param time The time to be set
	 */
	public void setCountdownTime(double time) {
		countdowntime = time;
	}
	
	/**
	 * Gets the countdown time
	 * @return The countdown time
	 */
	public double getCountdownTime() {
		return countdowntime;
	}
	
	/***
	 * Sets the maximum amount of players in the match
	 * @param maxplayers The maximum amount to be set
	 */
	public void setMaxPlayers(int maxplayers) {
		this.maxplayers = maxplayers;
	}
	
	/**
	 * Gets the maximum amount of players in the match
	 * @return The maximum amount of players
	 */
	public int getMaxPlayers() {
		return maxplayers;
	}
	
	/**
	 * Sets the ingame status of the match
	 * @param ingame ingame status
	 */
	public void setIngameStatus(boolean ingame) {
		this.ingame = ingame;
	}
	
	/**
	 * Checks if the match is running
	 * @return is the match running?
	 */
	public boolean isIngame() {
		return ingame;
	}
	
	/**
	 * Adds a participant to the match
	 * @param player
	 */
	public void addParticipant(Player player) {
		participants.addPlayer(player);
		player.teleport(lobbyspawn);
	}
	
	/**
	 * Removes a participant from the match
	 * @param player
	 */
	public void removeParticipant(Player player) {
		participants.removePlayer(player);
	}
	
	/**
	 * Gets the participants of the match
	 * @return The participants
	 */
	public PlayerList getParticipants() {
		return participants;
	}
	
	/**
	 * Begins the match
	 */
	@SuppressWarnings("unused")
	public void beginMatch() {
		setIngameStatus(true);
		for(Player player : participants.getPlayers()) {
			// TODO Add action to do with the participants when the game starts
		}
	}
	
	/**
	 * Begins the match if the countdown is on 0 or below
	 */
	public void checkCountdown() {
		if(countdowntime <= 0) {
			beginMatch();
		}
	}
	
	/**
	 * Gets the plugin to which the match belongs
	 * @return The plugin to which the match belongs
	 */
	public MiniGamesPlugin getMiniGamesPlugin() {
		return plugin;
	}
}
